# Examples for anyBSM Mathematica interface

A selection of example notebooks reads:

 * [BasicExamples.nb](https://anybsm.gitlab.io/examples/MathematicaExamples/BasicExamples.html):
   * basic tutorial for most of the anyBSM Mathematica functions and how to obtain further help
   * obtains analytic results for $\lambda_{hhh}$ at one-loop in the SM
   * link to PackageX/CollierLink (analytic results for one-loop functions)
 * [SSM_CrosscheckLiterature.nb](https://anybsm.gitlab.io/examples/MathematicaExamples/SSM_CrosscheckLiterature.html)
   * analytic check of $\lambda_{hhh}$ in the singlet extended SM (SSM)
 * [SM_TopSelf.nb](https://anybsm.gitlab.io/examples/MathematicaExamples/SM_TopSelf.html)
   * analytic cross-check for top-quark self-energy in the SM
 * [SM_UV_finiteness.nb](https://anybsm.gitlab.io/examples/MathematicaExamples/SM_UV_finiteness.html)
   * analytic check for UV-finiteness of $\lambda_{hhh}$ in the SM
 * [SSM_UV_finiteness.nb](https://anybsm.gitlab.io/examples/MathematicaExamples/SSM_UV_finiteness.html)
   * analytic check for UV-finiteness of $\lambda_{hhh}$ in the SSM
 * [THDM_UV_finiteness.nb](https://anybsm.gitlab.io/examples/MathematicaExamples/THDMII_UV_finiteness.html)
   * analytic check for UV-finiteness of $\lambda_{hhh}$ in the THDM
  

All notebooks can be downloaded [here](https://gitlab.com/anybsm/anybsm_examples/-/tree/main/MathematicaExamples).
