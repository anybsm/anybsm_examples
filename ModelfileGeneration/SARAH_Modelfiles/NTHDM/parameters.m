(* ::Package:: *)

delts[i_]:=Sequence[
              Form -> Diagonal,
              DependenceNum -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              DependenceSPheno -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              Dependence -> Table[KroneckerDelta[a,b],{a,i},{b,i}],
              DependenceOptional -> Table[KroneckerDelta[a,b],{a,i},{b,i}]];

RCPeven = {{                ca1 ca2,                 sa1 ca2,     sa2},
           {- sa1 ca3 - ca1 sa2 sa3,   ca1 ca3 - sa1 sa2 sa3, ca2 sa3},
           {  sa1 sa3 - ca1 sa2 ca3, - sa1 sa2 ca3 - ca1 sa3, ca2 ca3}};
RCPodd = {{Cos[b], Sin[b]}, {-Sin[b], Cos[b]}};
Rcharged = {{Cos[b], Sin[b]}, {-Sin[b], Cos[b]}};

ParameterDefinitions = { 

{g1,        { Description -> "Hypercharge-Coupling"}},
{g2,        { Description -> "Left-Coupling"}},
{g3,        { Description -> "Strong-Coupling"}},    
{AlphaS,    {Description -> "Alpha Strong"}},	
{e,         { Description -> "electric charge"}}, 

{aEWinv,    { Description -> "inverse weak coupling constant at mZ"}},

{Yu,        { Description -> "Up-Yukawa-Coupling",
            Form -> Diagonal,
              Real -> True, 
			 DependenceNum ->  Sqrt[2]/v2* {{Mass[Fu,1],0,0},
             									  {0, Mass[Fu,2],0},
             									  {0, 0, Mass[Fu,3]}}}}, 
             									
{Yd,        { Description -> "Down-Yukawa-Coupling",
            Form -> Diagonal,
              Real -> True, 
			  DependenceNum ->  Sqrt[2]/v1* {{Mass[Fd,1],0,0},
             									   {0, Mass[Fd,2],0},
             									   {0, 0, Mass[Fd,3]}}}},
             									
{Ye,        { Description -> "Lepton-Yukawa-Coupling",
            Form -> Diagonal,
              Real -> True, 
			  DependenceNum ->  Sqrt[2]/v1* {{Mass[Fe,1],0,0},
             								   	{0, Mass[Fe,2],0},
             								   	{0, 0, Mass[Fe,3]}}}}, 
                                                                            
                                                                           
{Lambda1,    { LaTeX -> "\\lambda_1",
               OutputName -> Lam1,
               Real -> True, 
               LesHouches -> {HMIX,31},
               DependenceNum ->  1/((v1^2+v2^2)*Cos[ArcTan[TanBeta]]^2)*(-(M12/(Sin[ArcTan[TanBeta]]*Cos[ArcTan[TanBeta]]))* Sin[ArcTan[TanBeta]]^2+Mass[hh, 1]^2*((ca1*ca2))^2+Mass[hh, 2]^2*((-(ca1*sa2*sa3+sa1*ca3)))^2+Mass[hh, 3]^2*((-ca1*sa2*ca3+sa1*sa3))^2)}},
{Lambda2,    { LaTeX -> "\\lambda_2",
               OutputName -> Lam2,
               Real -> True, 
               LesHouches -> {HMIX,32},
               DependenceNum -> 1/((v1^2+v2^2)*Sin[ArcTan[TanBeta]]^2)*(-(M12/(Sin[ArcTan[TanBeta]]*Cos[ArcTan[TanBeta]]))* Cos[ArcTan[TanBeta]]^2+Mass[hh, 1]^2*((sa1*ca2))^2+Mass[hh, 2]^2*((ca1*ca3-sa1*sa2*sa3))^2+Mass[hh, 3]^2*((-(ca1*sa3+sa1*sa2*ca3)))^2)}},
{Lambda3,    { LaTeX -> "\\lambda_3",
               OutputName -> Lam3,
               Real -> True, 
               LesHouches -> {HMIX,33},
               DependenceNum -> 1/(v1^2+v2^2)*(-(M12/(Sin[ArcTan[TanBeta]]*Cos[ArcTan[TanBeta]]))+1/(Sin[ArcTan[TanBeta]]*Cos[ArcTan[TanBeta]])*(Mass[hh, 1]^2*((ca1*ca2)*(sa1*ca2))+Mass[hh, 2]^2*(-(ca1*sa2*sa3+sa1*ca3))*(ca1*ca3-sa1*sa2*sa3)+Mass[hh, 3]^2*(-ca1*sa2*ca3+sa1*sa3)*(-(ca1*sa3+sa1*sa2*ca3)))+2*Mass[Hm, 2]^2)}},
{Lambda4,    { LaTeX -> "\\lambda_4",
               OutputName -> Lam4,
               Real -> True, 
               LesHouches -> {HMIX,34},
               DependenceNum -> 1/(v1^2+v2^2)*((M12/(Sin[ArcTan[TanBeta]]*Cos[ArcTan[TanBeta]]))+Mass[Ah, 2]^2-2*Mass[Hm, 2]^2)}},

{Lambda5,    { LaTeX -> "\\lambda_5",
               OutputName -> Lam5,
               Real -> True, 
               LesHouches -> {HMIX,35},
               DependenceNum -> 1/(v1^2+v2^2)*((M12/(Sin[ArcTan[TanBeta]]*Cos[ArcTan[TanBeta]]))-Mass[Ah, 2]^2)}},

{Lambda6,    { LaTeX -> "\\lambda_6",
               OutputName -> Lam6,
               Real -> True, 
               LesHouches -> {HMIX,36},
               DependenceNum -> 1/vS^2 * (Mass[hh, 1]^2*(sa2)^2+Mass[hh, 2]^2*(ca2*sa3)^2+Mass[hh, 3]^2*(ca2*ca3)^2)}},

{Lambda7,    { LaTeX -> "\\lambda_7",
               OutputName -> Lam7,
               Real -> True, 
               LesHouches -> {HMIX,37},
               DependenceNum -> 1/(Sqrt[(v1^2+v2^2)]*vS*Cos[ArcTan[TanBeta]])*(Mass[hh, 1]^2*(ca1*ca2)*(sa2)+Mass[hh, 2]^2*(-(ca1*sa2*sa3+sa1*ca3))*(ca2*sa3)+Mass[hh, 3]^2*(-ca1*sa2*ca3+sa1*sa3)*(ca2*ca3))}},
               
{Lambda8,    { LaTeX -> "\\lambda_8",
               OutputName -> Lam8,
               Real -> True, 
               LesHouches -> {HMIX,38},
               DependenceNum -> 1/(Sqrt[(v1^2+v2^2)]*vS*Sin[ArcTan[TanBeta]])*(Mass[hh, 1]^2*(sa1*ca2)*(sa2)+Mass[hh, 2]^2*(ca1*ca3-sa1*sa2*sa3)*(ca2*sa3)+Mass[hh, 3]^2*(-(ca1*sa3+sa1*sa2*ca3))*(ca2*ca3))}},

{M112,      {  LaTeX -> "m^2_1",
               OutputName -> M112,
               Real -> True, 
               LesHouches -> {HMIX,20}}},

{M222,      {  LaTeX -> "m^2_2",
               OutputName -> M222,
               Real -> True, 
               LesHouches -> {HMIX,21}}},

{M12,       {  LaTeX -> "m_{12}",
               OutputName -> M12,
               Real -> True, 
               Value -> 40000,
               LesHouches -> {HMIX,22}}},
               
{MS2,       {  LaTeX -> "m_S^2",
               Real -> True, 
               OutputName -> MS2,
               LesHouches -> {HMIX,23}}},


{v1,        { Real -> True, Description -> "Down-VEV", LaTeX -> "v_1", DependenceNum -> v*Cos[b]}}, 
{v2,        { Real -> True, Description -> "Up-VEV", LaTeX -> "v_2", DependenceNum -> v*Sin[b]}},  
{vS,        { Real -> True, Description -> "Singlet-VEV", LaTeX -> "v_S", Value -> 300}},         
{v,         { Real -> True, Description -> "EW-VEV", OutputName -> vSM,
               DependenceNum -> 2*Mass[VWm]/e*Sqrt[1-Mass[VWm]^2/Mass[VZ]^2]
            }},
             
{b,         { Real -> True, Description -> "Pseudo Scalar mixing angle", DependenceNum -> ArcTan[TanBeta], OutputName -> betaH  }},             
{TanBeta,   { Real -> True, Description -> "Tan Beta", OutputName -> TanBeta, Value -> 1 }},
{sa1,       { Real -> True, DependenceNum -> Sin[a1]}},  
{ca1,       { Real -> True, DependenceNum -> Cos[a1]}},  
{sa2,       { Real -> True, DependenceNum -> Sin[a2]}},  
{ca2,       { Real -> True, DependenceNum -> Cos[a2]}},  
{sa3,       { Real -> True, DependenceNum -> Sin[a3]}},  
{ca3,       { Real -> True, DependenceNum -> Cos[a3]}},  
{a1,        { Real -> True, OutputName -> alpha1, LesHouches -> {HMIX,39}, Description -> "Scalar mixing angle a1", OutputName -> alphaH1, Value -> 1.3 }},  
{a2,        { Real -> True, OutputName -> alpha2, LesHouches -> {HMIX,40}, Description -> "Scalar mixing angle a2", OutputName -> alphaH2, Value -> -1.1 }},  
{a3,        { Real -> True, OutputName -> alpha3, LesHouches -> {HMIX,41}, Description -> "Scalar mixing angle a3", OutputName -> alphaH3, Value -> -1.2 }},  

{ZH,        { Description->"Scalar-Mixing-Matrix",
               Real->True,
               OutputName -> ZH,
               DependenceOptional -> None,
               DependenceNum -> RCPeven	}},

{ZA,        { Description->"Pseudo-Scalar-Mixing-Matrix",
               Real->True,
               OutputName -> ZA,
               DependenceOptional -> None,
               DependenceNum -> RCPodd	}},
               
{ZP,        { Description->"Charged-Mixing-Matrix",
               Real->True,
               OutputName -> ZP,
               DependenceOptional -> None,
               DependenceNum -> Rcharged}},  


{ThetaW,    { Description -> "Weinberg-Angle",  DependenceNum -> ArcSin[Sqrt[1 - Mass[VWm]^2/Mass[VZ]^2]]}}, 

{ZZ, {Description ->   "Photon-Z Mixing Matrix"}},
{ZW, {Description -> "W Mixing Matrix" }},

{Vu,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Up-Mixing-Matrix"}},
{Vd,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Down-Mixing-Matrix"}},
{Uu,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Up-Mixing-Matrix"}},
{Ud,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Down-Mixing-Matrix"}},
{Ve,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Lepton-Mixing-Matrix"}},
{Ue,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Lepton-Mixing-Matrix"}}

 }; 
 

