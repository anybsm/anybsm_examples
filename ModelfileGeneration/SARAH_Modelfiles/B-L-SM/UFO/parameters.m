delts[i_]:=Sequence[
              Form -> Diagonal,                                                                                           
              DependenceNum -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],                                          
              DependenceSPheno -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],                                      
              Dependence -> Table[KroneckerDelta[a,b],{a,i},{b,i}],
              DependenceOptional -> Table[KroneckerDelta[a,b],{a,i},{b,i}]];

ParameterDefinitions = { 

{g1,        { Description -> "Hypercharge-Coupling"}},

{g2,        { Description -> "Left-Coupling"}},
{g3,        { Description -> "Strong-Coupling"}},    
{AlphaS,    {Description -> "Alpha Strong"}},	
{e,         { Description -> "electric charge"}},
{Gf,        { Description -> "Fermi's constant"}},
{aEWinv,    { Description -> "inverse weak coupling constant at mZ"}},
 

{Yu,        { Description -> "Up-Yukawa-Coupling",
              Real -> True,
			 DependenceNum ->  Sqrt[2]/vH* {{Mass[Fu,1],0,0},
             									{0, Mass[Fu,2],0},
             									{0, 0, Mass[Fu,3]}}}}, 
             									
{Yd,        { Description -> "Down-Yukawa-Coupling",
              Real -> True,
			  DependenceNum ->  Sqrt[2]/vH* {{Mass[Fd,1],0,0},
             									{0, Mass[Fd,2],0},
             									{0, 0, Mass[Fd,3]}}}},
             									
{Ye,        { Description -> "Lepton-Yukawa-Coupling",
              Real -> True,
			  DependenceNum ->  Sqrt[2]/vH* {{Mass[Fe,1],0,0},
             									{0, Mass[Fe,2],0},
             									{0, 0, Mass[Fe,3]}}}}, 

{g11p,        {Description -> "Mixed Gauge Coupling 2", Real -> True, DependenceNum -> 0}},
{g1p1,        {Description -> "Mixed Gauge Coupling 1", Real -> True, DependenceNum -> 0}},
{g1p,         {Description -> "B-L-Coupling", LaTeX -> "g_{B-L}", Value -> 0.2, Real -> True}},

{vH,          { Description -> "EW-VEV",
                DependenceNum -> 2*Mass[VWm]/e*Sqrt[1-Mass[VWm]^2/Mass[VZ]^2],
               OutputName -> vSM }},

{vX, {LaTeX -> "v_X",
      Real -> True,
      DependenceNum -> MZp/(2*g1p),
      OutputName -> vX}},

{ThetaW,    { Description -> "Weinberg-Angle"}},
{ThetaWp,  { Description -> "Theta'", DependenceNum -> 0 }},

{Mh1,        { Description -> "SM Higgs Mass Parameter", Value -> 125.1 }},
{Mh2,        { Description -> "second neutral higgs mass", Value -> 300}},

{MuP, {OutputName -> MUP,
      LaTeX -> "\\mu'",
      LesHouches -> {BL,10}}},

{mu2, {OutputName -> mu,
      LaTeX -> "\\mu_2",
      LesHouches -> {BL,11}}},



{L1,  {LaTeX -> "\\lambda_1",
      Real -> True,
      DependenceNum -> -((Cos[\[Alpha]]^2*Mass[hh, 1]^2)/vH^2) - (Mass[hh, 2]^2*Sin[\[Alpha]]^2)/vH^2,
      OutputName -> L1}},

{L2, {LaTeX -> "\\lambda_2",
      Real -> True,
      DependenceNum -> (-4*g1p^2*Cos[\[Alpha]]^2*Mass[hh, 2]^2)/MZp^2 - (4*g1p^2*Mass[hh, 1]^2*Sin[\[Alpha]]^2)/MZp^2,
      OutputName -> L2}},

{L3, {LaTeX -> "\\lambda_3",
      Real -> True,
      DependenceNum -> (-4*g1p*Cos[\[Alpha]]*Mass[hh, 1]^2*Sin[\[Alpha]])/(MZp*vH) + (4*g1p*Cos[\[Alpha]]*Mass[hh, 2]^2*Sin[\[Alpha]])/(MZp*vH),
      OutputName -> L3}},


{ZH,        { Description->"Scalar-Mixing-Matrix", 
              Real -> True,
              DependenceNum -> {{Cos[\[Alpha]], -Sin[\[Alpha]]}, {Sin[\[Alpha]], Cos[\[Alpha]]}}
            }},
{\[Alpha],  { Description -> "Scalar mixing angle",
              OutputName -> alphaH,
              Real -> True,
              LesHouches -> {HMIX, 3}
}},

{ZA,        { Description->"Pseudo-Scalar-Mixing-Matrix", 
              DependenceNum -> None   }},

{ZM, {Description -> "Neutrino-Mixing-Matrix"}},
{ZZ, {Description ->   "Photon-Z-Z' Mixing Matrix"}},
{ZW, {Description -> "W Mixing Matrix"}},

{Yx, {OutputName -> Yx,
      LaTeX -> "Y_x",
      LesHouches -> YX}},

{Yv, {OutputName -> Yv,
      LaTeX -> "Y_\\nu",
      LesHouches -> Yv}},


{Vu,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Up-Mixing-Matrix"}},
{Vd,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Down-Mixing-Matrix"}},
{Uu,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Up-Mixing-Matrix"}},
{Ud,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Down-Mixing-Matrix"}},
{Ve,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Lepton-Mixing-Matrix"}},
{Ue,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Lepton-Mixing-Matrix"}}     

 }; 
 

