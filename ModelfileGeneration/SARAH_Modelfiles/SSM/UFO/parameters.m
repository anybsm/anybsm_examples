zers=Sequence[DependenceNum -> 0.,
              Dependence -> 0.,
              DependenceOptional -> 0.];

delts[i_]:=Sequence[
              Form -> Diagonal,
              DependenceNum -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              DependenceSPheno -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              Dependence -> Table[KroneckerDelta[a,b],{a,i},{b,i}],
              DependenceOptional -> Table[KroneckerDelta[a,b],{a,i},{b,i}]];

six2twoM=Table[s2t[i, j] (KroneckerDelta[3, i, j] + KroneckerDelta[6, i, j] + KroneckerDelta[3, i] KroneckerDelta[6, j] +KroneckerDelta[3, j] KroneckerDelta[6, i]),{i,6},{j,6}];    
six2two[symb_]:=Sequence[
              Dependence -> (six2twoM/.s2t->symb),
              DependenceNum -> (six2twoM/.s2t->symb),
              DependenceOptional -> (six2twoM/.s2t->symb)];

massdep[field_]:={{(Sqrt[2]*Mass[field, 1])/v, 0, 0}, {0, (Sqrt[2]*Mass[field, 2])/v, 0}, {0, 0, (Sqrt[2]*Mass[field, 3])/v}};


ParameterDefinitions = { 

{g1,        { Description -> "Hypercharge-Coupling"}},
{g2,        { Description -> "Left-Coupling"}},
{g3,        { Description -> "Strong-Coupling"}},    
{AlphaS,    {Description -> "Alpha Strong"}},	
{e,         { Description -> "electric charge"}}, 

{Gf,        { Description -> "Fermi's constant"}},
{aEWinv,    { Description -> "inverse weak coupling constant at mZ"}},

{Yu,        { Description -> "Up-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fu],
			 DependenceNum -> massdep[Fu]}},
             									
{Yd,        { Description -> "Down-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fd],
			 DependenceNum -> massdep[Fd]}},
              									
{Ye,        { Description -> "Lepton-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fe],
			 DependenceNum -> massdep[Fe]}},
                                                                            
                                                                           
{mu2,         { Description -> "SM Mu Parameter",
                OutputName->m2SM}},

{K2, {LaTeX -> "\\lambda_{SH}", Real -> True, DependenceNum -> -(K1/vS) + (Cos[\[Alpha]]*Mass[hh, 1]^2*Sin[\[Alpha]])/(v*vS) - (Cos[\[Alpha]]*Mass[hh, 2]^2*Sin[\[Alpha]])/(v*vS), OutputName -> LamSH}},

{LambdaS, {LaTeX -> "\\lambda_S", Real -> True, DependenceNum -> (K1*v^2)/(8*vS^3) - \[Kappa]/(4*vS) + (Cos[\[Alpha]]^2*Mass[hh, 2]^2)/(4*vS^2) + (Mass[hh, 1]^2*Sin[\[Alpha]]^2)/(4*vS^2), OutputName -> LamS}},

{\[Lambda], {LaTeX -> "\\lambda_H", Real -> True, DependenceNum -> (Cos[\[Alpha]]^2*Mass[hh, 1]^2)/v^2 + (Mass[hh, 2]^2*Sin[\[Alpha]]^2)/v^2, OutputName -> Lam}},

{ZH,     {Description -> "Scalar-Mixing-Matrix", 
          DependenceNum -> {{Cos[\[Alpha]], Sin[\[Alpha]]}, {-Sin[\[Alpha]],Cos[\[Alpha]]}},
          OutputName -> ZH,
          Real->True
    }},

{\[Alpha],  { Description -> "Scalar mixing angle",
              DependenceNum -> ArcTan[Tan2Alpha]/2,
              OutputName -> alphaH
    }},

{Tan2Alpha,  { Description -> "Tangens of scalar mixing angle",
              Value -> 0.1,
              Real->True,
              OutputName -> Tan2Alpha,
              LesHouches -> {HMIX,33}
    }},

{v,          { Description -> "EW-VEV",
               DependenceNum -> 2*Mass[VWp]/e*Sqrt[1-Mass[VWp]^2/Mass[VZ]^2],
               DependenceSPheno -> None,
               OutputName -> vSM}},

{ThetaW,    { Description -> "Weinberg-Angle",
              DependenceNum -> ArcSin[Sqrt[1 - Mass[VWp]^2/Mass[VZ]^2]]  }},

{ZZ, {Description -> "Photon-Z Mixing Matrix"}},
{ZW, {Description -> "W Mixing Matrix",
       Dependence ->   1/Sqrt[2] {{1, 1},
                  {\[ImaginaryI],-\[ImaginaryI]}} }},
                  
{vS,{ 
     Dependence -> None, 
     DependenceNum -> None, 
     DependenceOptional -> None, 
     DependenceSPheno -> None, 
     Real -> True,
     Value-> 300.0,
     LesHouches -> {HMIX, 51}, 
     LaTeX -> "vS", 
     OutputName -> vS}},                   


{K1,    { LaTeX -> "\\kappa_{SH}",
               OutputName -> KapSH,
               Real->True,
               Value -> 10,
               LesHouches -> {HMIX,31}}},
{\[Kappa],    { LaTeX -> "\\kappa_s",
               OutputName -> KapS,
               Real -> True,
               Value -> 10.0,
               LesHouches -> {HMIX,35}}},               

{MS,    { LaTeX -> "M_S",
               OutputName -> MuS,
               LesHouches -> {HMIX,34}}},

{Vu,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Up-Mixing-Matrix"}},
{Vd,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Down-Mixing-Matrix"}},
{Uu,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Up-Mixing-Matrix"}},
{Ud,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Down-Mixing-Matrix"}}, 
{Ve,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Lepton-Mixing-Matrix"}},
{Ue,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Lepton-Mixing-Matrix"}}(*,

{ZEL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Lepton-Mixing-Matrix"}},
{ZER,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Lepton-Mixing-Matrix" }},
{ZDL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Down-Mixing-Matrix"}},                              
{ZDR,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Down-Mixing-Matrix"}},                      
{ZUL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Up-Mixing-Matrix"}},                        
{ZUR,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Up-Mixing-Matrix"}}*)
 }; 
 

