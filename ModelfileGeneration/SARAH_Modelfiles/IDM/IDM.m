Off[General::spell]

Model`Name = "IDM";
Model`NameLaTeX ="Inert doublet Model";
Model`Authors = "B.Herrmann, F.Staub";
Model`Date = "2014-11-06";

(* 2013-09-01: changing to new conventions for FermionFields/MatterFields *)
(* 2014-04-24: removed mixing between neutral Higgs fields *)
(* 2014-11-06: Changed sign in Lagrangian *)


(*-------------------------------------------*)
(*   Particle Content*)
(*-------------------------------------------*)

(* Global Symmetries *)

Global[[1]] = {Z[2], Z2};

(* Gauge Groups *)

Gauge[[1]]={B,   U[1], hypercharge, g1,False,1};
Gauge[[2]]={WB, SU[2], left,        g2,True, 1};
Gauge[[3]]={G,  SU[3], color,       g3,False,1};


(* Matter Fields *)

FermionFields[[1]] = {q, 3, {uL, dL},     1/6, 2,  3, 1};  
FermionFields[[2]] = {l, 3, {vL, eL},    -1/2, 2,  1, 1};
FermionFields[[3]] = {d, 3, conj[dR],     1/3, 1, -3, 1};
FermionFields[[4]] = {u, 3, conj[uR],    -2/3, 1, -3, 1};
FermionFields[[5]] = {e, 3, conj[eR],       1, 1,  1, 1};

ScalarFields[[1]] =  {H1, 1, {Gpp, H10},    1/2, 2,  1,  1};
ScalarFields[[2]] =  {H2, 1, {Hp, H20},     1/2, 2,  1, -1};




(*----------------------------------------------*)
(*   DEFINITION                                 *)
(*----------------------------------------------*)

NameOfStates={GaugeES, EWSB};

(* ----- Before EWSB ----- *)


  		  
DEFINITION[GaugeES][Additional]= {
	{LagHC, {AddHC->True}},
	{LagNoHC,{ AddHC->False}}
};



LagNoHC = -(mu12 conj[H1].H1 + mu22 conj[H2].H2 + Lambda1/2 conj[H1].H1.conj[H1].H1 + \
		Lambda2/2 conj[H2].H2.conj[H2].H2 + Lambda3 conj[H1].H1.conj[H2].H2  + Lambda4 conj[H2].H1.H2.conj[H1]);


LagHC = -(Lambda5/2 conj[H1].H2.conj[H1].H2 + Yd conj[H1].q.d + Ye conj[H1].l.e - Yu H1.q.u);



DEFINITION[EWSB][GaugeSector] =
{ 
  {{VB,VWB[3]},{VP,VZ},ZZ},
  {{VWB[1],VWB[2]},{VWp,conj[VWp]},ZW}
};    
        
        
          	

(* ----- VEVs ---- *)


DEFINITION[EWSB][VEVs]= 
{    {H10, {v, 1/Sqrt[2]}, {G0, \[ImaginaryI]/Sqrt[2]},{hh, 1/Sqrt[2]}},
     {H20, {0, 0}, {A0, \[ImaginaryI]/Sqrt[2]},{H0, 1/Sqrt[2]}}     };

 

(* ---- Mixings ---- *)


DEFINITION[EWSB][MatterSector]= 
{    
     {{{dL}, {conj[dR]}}, {{DL,ZDL}, {DR,ZDR}}},
     {{{uL}, {conj[uR]}}, {{UL,ZUL}, {UR,ZUR}}},
     {{{eL}, {conj[eR]}}, {{EL,ZEL}, {ER,ZER}}}
           
 }; 


DEFINITION[EWSB][DiracSpinors]={
 Fd ->{  DL, conj[DR]},
 Fe ->{  EL, conj[ER]},
 Fu ->{  UL, conj[UR]},
 Fv ->{  vL, 0}};


DEFINITION[EWSB][GaugeES]={
 Fd1 ->{  FdL, 0},
 Fd2 ->{  0, FdR},
 Fu1 ->{  Fu1, 0},
 Fu2 ->{  0, Fu2},
 Fe1 ->{  Fe1, 0},
 Fe2 ->{  0, Fe2}};



