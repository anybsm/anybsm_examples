
ParticleDefinitions[GaugeES] = {
      
      {Gpp   ,  {  Description -> "Pseudo-Scalar Higgs",
               PDG -> {0},
               PDG.IX ->{0},
               Mass -> {0},
               FeynArtsNr -> 5,
               ElectricCharge->1,                 
               Width -> {0} }},

      
      {Hp,  { Description -> "Charged Higgs",
                 PDG -> {37},
                 PDG.IX ->{101000003},
                 ElectricCharge -> 1,
                 FeynArtsNr -> 6}},     
      

      

             {VB,   { Description -> "B-Boson"}},                                                   
      {VG,   { Description -> "Gluon"}},          
      {VWB,  { Description -> "W-Bosons"}},          
      {gB,   { Description -> "B-Boson Ghost"}},                                                   
      {gG,   { Description -> "Gluon Ghost" }},          
      {gWB,  { Description -> "W-Boson Ghost"}}
      };
      
      
      
      
   ParticleDefinitions[EWSB] = {

     {hh,   { Description -> "Higgs",
               PDG -> {25, 35},
               PDG.IX ->{101000001,101000002},
               Mass -> {125.1, 500.0},
               ElectricCharge -> 0,
               OutputName -> "h" }}, 

     {Ah, {   Description -> "Pseudo-Scalar Higgs",
                 PDG -> {0, 36},
                 PDG.IX ->{0,102000001},
                 Width -> {0, External},
                 Mass ->LesHouches,
                 ElectricCharge -> 0,
                 OutputName -> "Ah" }},                       
     
      {Hm,  { Description -> "Charged Higgs",
              Mass ->LesHouches,
              PDG -> {0, 37},
              Width -> {0,External}}},   
                 
                                                   
      {VP,   { Description -> "Photon"}}, 
      {VZ,   { Description -> "Z-Boson", MassDependence -> Mass[VZ], Mass->91.187, Goldstone -> Ah[{1}] }}, 
      {VG,   { Description -> "Gluon" }},          
      {VWp,  { Description -> "W+ - Boson",MassDependence -> Mass[VWp], Mass-> 80.379, Goldstone -> Hm[{1}]}},         
      {gP,   { Description -> "Photon Ghost"}},                                                   
      {gWp,  { Description -> "Positive W+ - Boson Ghost"}}, 
      {gWpC, { Description -> "Negative W+ - Boson Ghost" }}, 
      {gZ,   { Description -> "Z-Boson Ghost" }},
      {gG,   { Description -> "Gluon Ghost" }}, 
                               
                 
      {Fd,   { Description -> "Down-Quarks"}},   
      {Fu,   { Description -> "Up-Quarks"}},   
      {Fe,   { Description -> "Leptons" }},
      {Fv,   { Description -> "Neutrinos" }}                                                              
     
        };    
        
        
        
 WeylFermionAndIndermediate = {
                 
       {phid,   {  PDG -> 0,
                 Width -> 0, 
                 Mass -> Automatic,
                 LaTeX -> "\\phi_{d}",
                 OutputName -> "" }},                                                                       
   
   
       {phiu,   {  PDG -> 0,
                 Width -> 0, 
                 Mass -> Automatic,
                 LaTeX -> "\\phi_{u}",
                 OutputName -> "" }}, 
                                                                                       
    {sigmad,   {  PDG -> 0,
                 Width -> 0, 
                 Mass -> Automatic,
                 LaTeX -> "\\sigma_{d}",
                 OutputName -> "" }},
                                                                                        
    {sigmau,   {  PDG -> 0,
                 Width -> 0, 
                 Mass -> Automatic,
                 LaTeX -> "\\sigma_{u}",
                 OutputName -> "" }}


        };       


