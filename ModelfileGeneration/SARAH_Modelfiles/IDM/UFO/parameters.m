delts[i_]:=Sequence[
              Form -> Diagonal,
              DependenceNum -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              DependenceSPheno -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              Dependence -> Table[KroneckerDelta[a,b],{a,i},{b,i}],
              DependenceOptional -> Table[KroneckerDelta[a,b],{a,i},{b,i}]];

massdep[field_,v_:v]:={{(Sqrt[2]*Mass[field, 1])/v, 0, 0}, {0, (Sqrt[2]*Mass[field, 2])/v, 0}, {0, 0, (Sqrt[2]*Mass[field, 3])/v}};

ParameterDefinitions = { 

{g1,        { Description -> "Hypercharge-Coupling"}},
{g2,        { Description -> "Left-Coupling"}},
{g3,        { Description -> "Strong-Coupling"}},  
				
{AlphaS,    {Description -> "Alpha Strong"}},

{Gf,        { Description -> "Fermi's constant"}},
{aEWinv,    { Description -> "inverse weak coupling constant at mZ"}},
{e,         { Description -> "electric charge"}}, 

{Yu,        { Description -> "Up-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fu,v],
			 DependenceNum -> massdep[Fu,v]}},
             									
{Yd,        { Description -> "Down-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fd,v],
			 DependenceNum -> massdep[Fd,v]}},
              									
{Ye,        { Description -> "Lepton-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fe,v],
			 DependenceNum -> massdep[Fe,v]}},


{mu12,      { Description -> "Softbreaking Down-Higgs Mass"}}, 
{mu22,      { Description -> "Softbreaking Up-Higgs Mass"}}, 


{v,         { Description -> "EW-VEV",
               DependenceNum -> 2*Mass[VWp]/e*Sqrt[1-Mass[VWp]^2/Mass[VZ]^2],
               DependenceSPheno -> None ,
               OutputName -> "vSM"}},           

{ZH,        { Description->"Scalar-Mixing-Matrix",
              Real->True,
              OutputName -> ZH,
              DependenceNum -> {{1,0},{0,1}}
            }},
{ZA,        { Description->"Pseudo-Scalar-Mixing-Matrix",
              Real->True,
              OutputName -> ZA,
              DependenceNum -> {{1,0}, {0,1}}
            }},
{ZP,        { Description->"Charged-Mixing-Matrix",
              Real-> True,
              OutputName -> ZP,
              DependenceNum -> {{1,0}, {0,1}}
            }},                      

{\[Alpha],  { Description -> "Scalar mixing angle",
              Real -> True,
              OutputName -> alphaH,
              Value -> 0
            }},
{\[Beta],   { Description -> "pseudo scalar mixing",
              Real -> True,
              OutputName -> betaH,
              Value -> 0
            }},                                        
 
{ZEL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Lepton-Mixing-Matrix"}},
{ZER,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Lepton-Mixing-Matrix" }},
{ZDL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Down-Mixing-Matrix"}},                              
{ZDR,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Down-Mixing-Matrix"}},                      
{ZUL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Up-Mixing-Matrix"}},                        
{ZUR,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Up-Mixing-Matrix"}},           
              
{ThetaW,    { Description -> "Weinberg-Angle",
              DependenceNum -> ArcSin[Sqrt[1 - Mass[VWp]^2/Mass[VZ]^2]] }},                           
{ZZ, {Description ->   "Photon-Z Mixing Matrix"}},
{ZW, {Description -> "W Mixing Matrix",
       Dependence ->   1/Sqrt[2] {{1, 1},
                  {\[ImaginaryI],-\[ImaginaryI]}} }},

{Lambda1,   { OutputName ->"Lam1",
              LesHouches -> {HDM,1},
              DependenceNum -> Mass[hh,1]^2/(v^2),
              Real -> True}},

{Lambda2,   { OutputName ->"Lam2",
              LesHouches -> {HDM,2}, Real->True}},

{Lambda3,   { OutputName ->"Lam3",
              LesHouches -> {HDM,3},
              DependenceNum -> 2 (Mass[Hm, 2]^2 - mu22) / v^2,
              Real->True}},

{Lambda4,   { OutputName ->"Lam4",
              LesHouches -> {HDM,4},
              DependenceNum -> (2 Mass[Hm, 2]^2 - Mass[Ah,2]^2 - Mass[hh,2]^2) / v^2,
              Real->True}},

{Lambda5,   { Real->True,
              OutputName ->"Lam5",
              LesHouches -> {HDM,5},
              DependenceNum -> (Mass[hh,2]^2 - Mass[Ah,2]^2) / v^2}}

 }; 
 

