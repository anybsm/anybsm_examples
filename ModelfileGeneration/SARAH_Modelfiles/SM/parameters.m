zers=Sequence[DependenceNum -> 0.,
              Dependence -> 0.,
              DependenceOptional -> 0.];

delts[i_]:=Sequence[
              Form -> Diagonal,
              DependenceNum -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              DependenceSPheno -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              Dependence -> Table[KroneckerDelta[a,b],{a,i},{b,i}],
              DependenceOptional -> Table[KroneckerDelta[a,b],{a,i},{b,i}]];

six2twoM=Table[s2t[i, j] (KroneckerDelta[3, i, j] + KroneckerDelta[6, i, j] + KroneckerDelta[3, i] KroneckerDelta[6, j] +KroneckerDelta[3, j] KroneckerDelta[6, i]),{i,6},{j,6}];    
six2two[symb_]:=Sequence[
              Dependence -> (six2twoM/.s2t->symb),
              DependenceNum -> (six2twoM/.s2t->symb),
              DependenceOptional -> (six2twoM/.s2t->symb)];

massdep[field_]:={{(Sqrt[2]*Mass[field, 1])/v, 0, 0}, {0, (Sqrt[2]*Mass[field, 2])/v, 0}, {0, 0, (Sqrt[2]*Mass[field, 3])/v}};


ParameterDefinitions = { 

{g1,        { Description -> "Hypercharge-Coupling"}},
{g2,        { Description -> "Left-Coupling"}},
{g3,        { Description -> "Strong-Coupling"}},    
{AlphaS,    {Description -> "Alpha Strong"}},	
{e,         { Description -> "electric charge"}}, 

{Gf,        { Description -> "Fermi's constant"}},
{aEWinv,    { Description -> "inverse weak coupling constant at mZ"}},

{Yu,        { Description -> "Up-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fu],
			 DependenceNum -> massdep[Fu]}},
             									
{Yd,        { Description -> "Down-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fd],
			 DependenceNum -> massdep[Fd]}},
              									
{Ye,        { Description -> "Lepton-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fe],
			 DependenceNum -> massdep[Fe]}},
                                                                            
                                                                           
{mu2,         { Description -> "SM Mu Parameter",
                OutputName->m2SM}},                                        
{\[Lambda],  { Description -> "SM Higgs Selfcouplings",
               DependenceNum -> Mass[hh]^2/(v^2)}},
{v,          { Description -> "EW-VEV",
               DependenceNum -> 2*Mass[VWp]/e*Sqrt[1-Mass[VWp]^2/Mass[VZ]^2],
               DependenceSPheno -> None,
               OutputName -> vvSM}},
{mH2,        { Description -> "SM Higgs Mass Parameter"}, Value -> 125.1 },

{ThetaW,    { Description -> "Weinberg-Angle",
              DependenceNum -> ArcSin[Sqrt[1 - Mass[VWp]^2/Mass[VZ]^2]]  }},

{ZZ, {Description -> "Photon-Z Mixing Matrix"}},
{ZW, {Description -> "W Mixing Matrix",
       Dependence ->   1/Sqrt[2] {{1, 1},
                  {\[ImaginaryI],-\[ImaginaryI]}} }},


{Vu,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Up-Mixing-Matrix"}},
{Vd,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Down-Mixing-Matrix"}},
{Uu,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Up-Mixing-Matrix"}},
{Ud,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Down-Mixing-Matrix"}}, 
{Ve,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Lepton-Mixing-Matrix"}},
{Ue,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Lepton-Mixing-Matrix"}}(*,

{ZEL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Lepton-Mixing-Matrix"}},
{ZER,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Lepton-Mixing-Matrix" }},
{ZDL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Down-Mixing-Matrix"}},                              
{ZDR,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Down-Mixing-Matrix"}},                      
{ZUL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Up-Mixing-Matrix"}},                        
{ZUR,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Up-Mixing-Matrix"}}*)
 }; 
 

