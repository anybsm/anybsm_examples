zers=Sequence[DependenceNum -> 0.,
              Dependence -> 0.,
              DependenceOptional -> 0.];

delts[i_]:=Sequence[
              Form -> Diagonal,
              DependenceNum -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              DependenceSPheno -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              Dependence -> Table[KroneckerDelta[a,b],{a,i},{b,i}],
              DependenceOptional -> Table[KroneckerDelta[a,b],{a,i},{b,i}]];

six2twoM=Table[s2t[i, j] (KroneckerDelta[3, i, j] + KroneckerDelta[6, i, j] + KroneckerDelta[3, i] KroneckerDelta[6, j] +KroneckerDelta[3, j] KroneckerDelta[6, i]),{i,6},{j,6}];    
six2two[symb_]:=Sequence[
              Dependence -> (six2twoM/.s2t->symb),
              DependenceNum -> (six2twoM/.s2t->symb),
              DependenceOptional -> (six2twoM/.s2t->symb)];

massdep[field_,v_:v]:={{(Sqrt[2]*Mass[field, 1])/v, 0, 0}, {0, (Sqrt[2]*Mass[field, 2])/v, 0}, {0, 0, (Sqrt[2]*Mass[field, 3])/v}};


ParameterDefinitions = {

{g1,        { Description -> "Hypercharge-Coupling"}},
{g2,        { Description -> "Left-Coupling"}},
{g3,        { Description -> "Strong-Coupling"}},    
{AlphaS,    {Description -> "Alpha Strong"}},	
{e,         { Description -> "electric charge"}}, 

{Gf,        { Description -> "Fermi's constant"}},
{aEWinv,    { Description -> "inverse weak coupling constant at mZ"}},

{Yu,        { Description -> "Up-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fu,v2],
			 DependenceNum -> massdep[Fu,v2]}},
             									
{Yd,        { Description -> "Down-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fd,v2],
			 DependenceNum -> massdep[Fd,v2]}},
              									
{Ye,        { Description -> "Lepton-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fe,v1],
			 DependenceNum -> massdep[Fe,v1]}},

{Lambda1, {LaTeX -> "\\lambda_1", Real -> True, DependenceNum -> -1/2*(M^2*TanBeta^2)/v^2 + (SinBmA^2*Mass[hh, 1]^2)/(2*v^2) - 
     (SinBmA*Sqrt[1 - SinBmA^2]*TanBeta*Mass[hh, 1]^2)/v^2 + (TanBeta^2*Mass[hh, 1]^2)/(2*v^2) - (SinBmA^2*TanBeta^2*Mass[hh, 1]^2)/(2*v^2) + 
     Mass[hh, 2]^2/(2*v^2) - (SinBmA^2*Mass[hh, 2]^2)/(2*v^2) + (SinBmA*Sqrt[1 - SinBmA^2]*TanBeta*Mass[hh, 2]^2)/v^2 + 
     (SinBmA^2*TanBeta^2*Mass[hh, 2]^2)/(2*v^2), OutputName -> Lam1}}, 
 {Lambda2, {LaTeX -> "\\lambda_2", Real -> True, DependenceNum -> -1/2*M^2/(TanBeta^2*v^2) + (SinBmA^2*Mass[hh, 1]^2)/(2*v^2) + 
     Mass[hh, 1]^2/(2*TanBeta^2*v^2) - (SinBmA^2*Mass[hh, 1]^2)/(2*TanBeta^2*v^2) + (SinBmA*Sqrt[1 - SinBmA^2]*Mass[hh, 1]^2)/(TanBeta*v^2) + 
     Mass[hh, 2]^2/(2*v^2) - (SinBmA^2*Mass[hh, 2]^2)/(2*v^2) + (SinBmA^2*Mass[hh, 2]^2)/(2*TanBeta^2*v^2) - 
     (SinBmA*Sqrt[1 - SinBmA^2]*Mass[hh, 2]^2)/(TanBeta*v^2), OutputName -> Lam2}}, 
 {Lambda3, {LaTeX -> "\\lambda_3", Real -> True, DependenceNum -> -(M^2/v^2) - Mass[hh, 1]^2/v^2 + (2*SinBmA^2*Mass[hh, 1]^2)/v^2 + 
     (SinBmA*Sqrt[1 - SinBmA^2]*Mass[hh, 1]^2)/(TanBeta*v^2) - (SinBmA*Sqrt[1 - SinBmA^2]*TanBeta*Mass[hh, 1]^2)/v^2 + Mass[hh, 2]^2/v^2 - 
     (2*SinBmA^2*Mass[hh, 2]^2)/v^2 - (SinBmA*Sqrt[1 - SinBmA^2]*Mass[hh, 2]^2)/(TanBeta*v^2) + 
     (SinBmA*Sqrt[1 - SinBmA^2]*TanBeta*Mass[hh, 2]^2)/v^2 + (2*Mass[Hm, 2]^2)/v^2, OutputName -> Lam3}}, 
 {Lambda4, {LaTeX -> "\\lambda_4", Real -> True, DependenceNum -> M^2/v^2 + Mass[Ah, 2]^2/v^2 - (2*Mass[Hm, 2]^2)/v^2, OutputName -> Lam4}}, 
 {Lambda5, {LaTeX -> "\\lambda_5", Real -> True, DependenceNum -> M^2/v^2 - Mass[Ah, 2]^2/v^2, OutputName -> Lam5}},

{Lambda6,    { LaTeX -> "\\lambda_6",
               OutputName -> Lam6,
               DependenceNum -> 0
            }},

{Lambda7,    { LaTeX -> "\\lambda_7",
               OutputName -> Lam7,
               DependenceNum -> 0
            }},
{M112,    {    LaTeX -> "m^2_1",
               OutputName -> M112,
               DependenceNum -> -Lambda1 v1^2-(v2 (2 M12+(Lambda3+Lambda4+Lambda5) v1 v2))/(2 v1)
          }},

{M222,    {    LaTeX -> "m^2_2",
               OutputName -> M222,
               DependenceNum -> -((2 M12 v1+Lambda3 v1^2 v2+Lambda4 v1^2 v2+Lambda5 v1^2 v2+2 Lambda2 v2^3)/(2 v2))   
          }},

{M12,    {    LaTeX -> "m_{12}",
               OutputName -> M12,
               DependenceNum -> -M^2*Cos[\[Beta]]*Sin[\[Beta]]
          }},

{M,       {    LaTeX -> "M",
               OutputName -> M,
               Real -> True,
               LesHouches -> {HMIX, 20}
          }},

{v1,        { Description -> "vev 1",OutputName-> v1, LaTeX -> "v_1", Real -> True, DependenceNum -> Cos[\[Beta]]*v}}, 
{v2,        { Description -> "vev 2",OutputName-> v2, LaTeX -> "v_2", Real -> True, DependenceNum -> Sin[\[Beta]]*v}}, 

{v,          { Description -> "EW-VEV",
               DependenceNum -> 2*Mass[VWm]/e*Sqrt[1-Mass[VWm]^2/Mass[VZ]^2],
               DependenceSPheno -> None,
               OutputName -> vSM}},
            
{\[Beta],   { Description -> "pseudo scalar mixing",
              Real -> True,
              OutputName -> betaH,
              DependenceNum -> ArcTan[TanBeta]
            }},
{TanBeta,   { Description -> "TBeta",
              LaTeX -> "\\tan\\beta",
              Real -> True,
              OutputName -> TanBeta,
              LesHouches -> {HMIX, 23} 
            }},   
{\[Alpha],  { Description -> "Scalar mixing angle",
              Real -> True,
              OutputName -> alphaH,
              DependenceNum -> ArcTan[TanBeta]-ArcSin[SinBmA]
            }},  

{SinBmA,   { Description -> "SM alignment variable",
             Real -> True,
             OutputName -> SinBmA,
             LesHouches -> {HMIX, 22}
          }},

{ZH,        { Description->"Scalar-Mixing-Matrix",
              Real->True,
              OutputName -> ZH,
              (*DependenceNum -> {{Cos[\[Alpha]], -Sin[\[Alpha]]}, {Sin[\[Alpha]], Cos[\[Alpha]]}}*)
              (*swaps eigenvectors as they appears (h2,h1)-orderes in SARAH/Mathematica *)
              DependenceNum -> {{-Sin[\[Alpha]],Cos[\[Alpha]]},{Cos[\[Alpha]],Sin[\[Alpha]]}}
            }},
{ZA,        { Description->"Pseudo-Scalar-Mixing-Matrix",
              Real->True,
              OutputName -> ZA,
              DependenceNum -> {{Cos[\[Beta]], Sin[\[Beta]]}, {-Sin[\[Beta]], Cos[\[Beta]]}}
            }},
{ZP,        { Description->"Charged-Mixing-Matrix",
              Real-> True,
              OutputName -> ZP,
              DependenceNum -> {{Cos[\[Beta]], Sin[\[Beta]]}, {-Sin[\[Beta]], Cos[\[Beta]]}}
            }},

{ThetaW,    { Description -> "Weinberg-Angle",
              DependenceNum -> ArcSin[Sqrt[1 - Mass[VWm]^2/Mass[VZ]^2]]  }},

{ZZ, {Description -> "Photon-Z Mixing Matrix"}},
{ZW, {Description -> "W Mixing Matrix"}},
(*       Dependence ->   1/Sqrt[2] {{1, 1},
                  {\[ImaginaryI],-\[ImaginaryI]}} }},
*)

{Vu,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Up-Mixing-Matrix"}},
{Vd,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Down-Mixing-Matrix"}},
{Uu,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Up-Mixing-Matrix"}},
{Ud,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Down-Mixing-Matrix"}}, 
{Ve,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Lepton-Mixing-Matrix"}},
{Ue,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Lepton-Mixing-Matrix"}}(*,

{ZEL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Lepton-Mixing-Matrix"}},
{ZER,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Lepton-Mixing-Matrix" }},
{ZDL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Down-Mixing-Matrix"}},                              
{ZDR,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Down-Mixing-Matrix"}},                      
{ZUL,       {delts[3], Real->True,Form->Diagonal, Description ->"Left-Up-Mixing-Matrix"}},                        
{ZUR,       {delts[3], Real->True,Form->Diagonal, Description ->"Right-Up-Mixing-Matrix"}}*)
 }; 
 

