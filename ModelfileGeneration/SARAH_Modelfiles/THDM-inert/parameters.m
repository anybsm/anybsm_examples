ParameterDefinitions = { 

{g1,        { Description -> "Hypercharge-Coupling"}},
{g2,        { Description -> "Left-Coupling"}},
{g3,        { Description -> "Strong-Coupling"}},  
				
{AlphaS,    {Description -> "Alpha Strong"}},

{Gf,        { Description -> "Fermi's constant"}},
{aEWinv,    { Description -> "inverse weak coupling constant at mZ"}},
{e,         { Description -> "electric charge"}}, 

{Yu,        { Description -> "Up-Yukawa-Coupling"   }}, 
{Yd,        { Description -> "Down-Yukawa-Coupling"}},
{Ye,        { Description -> "Lepton-Yukawa-Coupling"}}, 


{MHD,      { Description -> "Softbreaking Down-Higgs Mass"}}, 
{MHU,      { Description -> "Softbreaking Up-Higgs Mass", value -> 500^2}}, 


{v,         { Description -> "EW-VEV",
               DependenceNum -> Sqrt[4*Mass[VWp]^2/(g2^2)],
               DependenceSPheno -> None }},           
{\[Beta],   { Description -> "Pseudo Scalar mixing angle"  }},             
{TanBeta,   { Description -> "Tan Beta" }},                                    
{Zp,        { Description->"Charged-Mixing-Matrix",
              Dependence -> IdentityMatrix[2]}},                                    
 


{ZEL,       { Description ->"Left-Lepton-Mixing-Matrix"}},
{ZER,       { Description ->"Right-Lepton-Mixing-Matrix" }},                          
{ZDL,       { Description ->"Left-Down-Mixing-Matrix"}},                       
{ZDR,       { Description ->"Right-Down-Mixing-Matrix"}},              
{ZUL,       { Description ->"Left-Up-Mixing-Matrix"}},                        
{ZUR,       { Description ->"Right-Up-Mixing-Matrix"}},           
              
{ThetaW,    { Description -> "Weinberg-Angle",
              DependenceNum -> ArcSin[Sqrt[1 - Mass[VWp]^2/Mass[VZ]^2]] }},                           
{ZZ, {Description ->   "Photon-Z Mixing Matrix"}},
{ZW, {Description -> "W Mixing Matrix",
       Dependence ->   1/Sqrt[2] {{1, 1},
                  {\[ImaginaryI],-\[ImaginaryI]}} }},

{Lambda1,   { OutputName ->"Lam1",
              DependenceNum -> Mass[hh]^2/(2 v^2),
              Real -> True}},

{Lambda2,   { OutputName ->"Lam2", Real->True}},

{Lambda3,   { OutputName ->"Lam3",
              DependenceNum -> 2 (Mass[Hp, 2]^2 - MHU) / v^2,
              Real -> True}},

{Lambda4,   { OutputName ->"Lam4",
              DependenceNum -> (2 Mass[Hp, 2]^2 - Mass[A0]^2 - Mass[hh]^2) / v^2}},

{Lambda5,   { OutputName ->"Lam5",
              DependenceNum -> (Mass[hh]^2 - Mass[A0]^2) / v^2}}

 }; 
 

