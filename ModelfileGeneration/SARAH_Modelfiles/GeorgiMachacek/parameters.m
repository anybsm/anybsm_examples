zers=Sequence[DependenceNum -> 0.,
              Dependence -> 0.,
              DependenceOptional -> 0.];

dep[expr_]:=Sequence[DependenceNum -> expr,
              DependenceSPheno -> expr,
              Dependence -> None,
              Value-> None,
              DependenceOptional -> expr];
delts[i_]:=Sequence[
              Form -> Diagonal,
              DependenceNum -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              DependenceSPheno -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              Dependence -> Table[KroneckerDelta[a,b],{a,i},{b,i}],
              DependenceOptional -> Table[KroneckerDelta[a,b],{a,i},{b,i}]];

six2twoM=Table[s2t[i, j] (KroneckerDelta[3, i, j] + KroneckerDelta[6, i, j] + KroneckerDelta[3, i] KroneckerDelta[6, j] +KroneckerDelta[3, j] KroneckerDelta[6, i]),{i,6},{j,6}];    
six2two[symb_]:=Sequence[
              Dependence -> (six2twoM/.s2t->symb),
              DependenceNum -> (six2twoM/.s2t->symb),
              DependenceOptional -> (six2twoM/.s2t->symb)];

massdep[field_,v_:v]:={{(Sqrt[2]*Mass[field, 1])/v, 0, 0}, {0, (Sqrt[2]*Mass[field, 2])/v, 0}, {0, 0, (Sqrt[2]*Mass[field, 3])/v}};


(*this parametrisation uses Mh1 Mh2, M1, M2 M3, M5 and alphaH, SinH as inputs while all dimensionless couplings in the lagrangian are derved quantities *)
ParameterDefinitions = { 
 
{g1,        { Description -> "Hypercharge-Coupling"}},
{g2,        { Description -> "Left-Coupling"}},
{g3,        { Description -> "Strong-Coupling"}},    
{AlphaS,    {Description -> "Alpha Strong"}},	
{e,         { Description -> "electric charge"}}, 

{Gf,        { Description -> "Fermi's constant"}},
{aEWinv,    { Description -> "inverse weak coupling constant at mZ"}},

                                                                         
{mu2,{ 
    Description -> "SM Mu Parameter", 
    LesHouches -> {HMIX, 2}, 
    OutputName -> Mu}},


{ThetaW,    { Description -> "Weinberg-Angle",
              DependenceNum -> ArcSin[Sqrt[1 - Mass[VWp]^2/Mass[VZ]^2]]  }},

{ZZ, {Description -> "Photon-Z Mixing Matrix"}},
{ZW, {Description -> "W Mixing Matrix", Dependence -> 1/Sqrt[2] {{1, 1}, {\[ImaginaryI],-\[ImaginaryI]}} }},

{SinH, {Real -> True, OutputName -> SinH, Value-> 0.1, LesHouches -> {MINPAR, 14}}},
{CosH, {Real -> True, OutputName -> CosH, dep[Sqrt[1-SinH^2]]}},


{vSM, {Description -> "EW-VEV",
      Real->True,
      dep[2*Mass[VWp]/e*Sqrt[1-Mass[VWp]^2/Mass[VZ]^2]],
      LaTeX -> "v_{SM}",
      OutputName -> vSM}},

{vX, {Real -> True, dep[(SinH*vSM)/(2*Sqrt[2])] , OutputName -> vEta, LaTeX -> "v_{X}" }},
{vDoub,{Real -> True, dep[Sqrt[-8*vX^2 + vSM^2]], OutputName -> vDoub, LaTeX -> "{v}_{\\Phi}"}},
{M2, {Real -> True, LesHouches -> {MINPAR, 2} , LaTeX -> "M_2" , OutputName -> M2, Value -> 600. }},

{M3, {Real -> True, OutputName -> M3, LaTeX -> "M_3", LesHouches -> {MINPAR, 13}, Value -> 500 }},
{M5, {Real -> True, OutputName -> M5, LaTeX -> "M_5", LesHouches -> {MINPAR, 15}, Value -> 500 }},
{Meta, {Real -> True, OutputName -> Meta, LaTeX -> "M_\\eta", LesHouches -> {MINPAR, 16}, Value -> 500^2 }},
{MXi,     {Real -> True, dep[Meta], OutputName -> MXi }},


{Lambda3, {Real -> True, Value -> -0.1, LesHouches-> {MINPAR,3}, OutputName -> Lam3, LaTeX -> "\\lambda_3"}},
{Lambda4, {Real -> True, Value -> 0.1, LesHouches-> {MINPAR,4}, OutputName -> Lam4, LaTeX -> "\\lambda_4"}},


{Lambda1, {Real -> True,
    dep[-1/4*(4*(-1 + SinH^2)*Mass[hh, 1]^2 + (3*SinH^2*(4*Meta^2 + 4*Meta*SinH*vSM*(-3*Sqrt[2]*M2 + (Lambda3 + 3*Lambda4)*SinH*vSM) + SinH^2*vSM^2*(18*M2^2 - 6*Sqrt[2]*(Lambda3 + 3*Lambda4)*M2*SinH*vSM + (Lambda3 + 3*Lambda4)^2*SinH^2*vSM^2)))/(M5^2 + 3*M3^2*(-1 + SinH^2) - 3*(Lambda3 + 2*Lambda4)*SinH^2*vSM^2 + 2*Mass[hh, 1]^2))/((-1 + SinH^2)^2*vSM^2)]
    ,OutputName -> Lam1, LaTeX -> "\\lambda_1"}},

{Lambda2, {Real -> True,
    dep[(-M5^2 + 2*Meta + M3^2*(-1 + SinH^2) + (2*Lambda3 + 3*Lambda4)*SinH^2*vSM^2)/(4*(-1 + SinH^2)*vSM^2)]
    , OutputName -> Lam2, LaTeX -> "\\lambda_2"}},

{Lambda5, { Real -> True,
     dep[-((M5^2 + M3^2*(-1 + SinH^2) - SinH*vSM*(3*Sqrt[2]*M2 + Lambda3*SinH*vSM))/((-1 + SinH^2)*vSM^2))],
     LaTeX -> "{\\lambda}_{5}", 
     OutputName -> Lam5 }}, 
{Lambda5c,     {Real -> True, dep[Lambda5],      OutputName -> Lam5c }},

{\[Alpha],  { Description -> "Scalar mixing angle",
             Real -> True,
             OutputName -> alphaH,
             LaTeX -> "\\alpha",
             dep[ArcTan[(Sqrt[3]*SinH*(2*Sqrt[2]*Meta + SinH*vSM*(-6*M2 + Sqrt[2]*(Lambda3 + 3*Lambda4)*SinH*vSM)))/(Sqrt[1 - SinH^2]*(2*Meta + vSM*(-6*Sqrt[2]*M2*SinH - 2*CosH^2*(Lambda1 - 2*Lambda2 + Lambda5)*vSM + 3*(Lambda3 + 3*Lambda4)*SinH^2*vSM)))]/2]
           }},

{ZH, {Real -> True, Description -> "Scalar-Mixing-Matrix",dep[{{Cos[\[Alpha]], Sin[\[Alpha]]/Sqrt[3], Sqrt[2/3]*Sin[\[Alpha]]}, {-Sin[\[Alpha]], Cos[\[Alpha]]/Sqrt[3], Sqrt[2/3]*Cos[\[Alpha]]}, {0, -Sqrt[2/3], 1/Sqrt[3]}}], OutputName -> ZH}}, 
{ZP, {Real -> True, Description -> "Charged-Mixing-Matrix", dep[{{CosH, SinH/Sqrt[2], SinH/Sqrt[2]}, {SinH, -(CosH/Sqrt[2]), -(CosH/Sqrt[2])}, {0, -(1/Sqrt[2]), 1/Sqrt[2]}}], OutputName -> ZP}},
{ZA, {Real -> True, Description -> "Pseudo-Scalar-Mixing-Matrix", dep[{{CosH, SinH}, {SinH, -CosH}}], OutputName -> ZA}}, 

{\[Beta],   { Description -> "pseudo scalar mixing",  Real -> True, LesHouches -> {MINPAR,0}, OutputName -> Beta}},

{M1,{Real -> True, dep[(4 vX (Meta+2 Lambda2 vDoub^2-Lambda5 vDoub^2-6 M2 vX+4 Lambda3 vX^2+12 Lambda4 vX^2))/vDoub^2], OutputName -> M1, LaTeX -> "M_1" }},
{M1c,     {Real -> True, dep[M1],      OutputName -> M1c }},

{Yu,        { Description -> "Up-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fu, vDoub],
			 DependenceNum -> massdep[Fu, vDoub]}},
             									
{Yd,        { Description -> "Down-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fd, vDoub],
			 DependenceNum -> massdep[Fd, vDoub]}},
              									
{Ye,        { Description -> "Lepton-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fe, vDoub],
			 DependenceNum -> massdep[Fe, vDoub]}},


 
{Vu,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Up-Mixing-Matrix"}},
{Vd,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Down-Mixing-Matrix"}},
{Uu,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Up-Mixing-Matrix"}},
{Ud,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Down-Mixing-Matrix"}}, 
{Ve,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Lepton-Mixing-Matrix"}},
{Ue,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Lepton-Mixing-Matrix"}}};

(* These mass definitions need to be added by hand
Mh2 = Parameter(name = 'Mh2',
    nature = 'internal',
    type = 'real',
    value = 'cmath.sqrt(-0.5*((M5**2 + Mh1**2 + 3*M3**2*(-1 + SinH**2) - 3*Lam3a*SinH**2*vSM**2 - 6*Lam4a*SinH**2*vSM**2 - Mh1**2*cmath.cos(2*alphaH))/cmath.cos(alphaH)**2))',
    texname = 'M_{{h}_2}',
    lhablock = 'MASS',
    lhacode = [35])
 
Mh3 = Parameter(name = 'Mh3',
    nature = 'internal',
    type = 'real',
    value = 'M5',
    texname = 'M_{{h}_3}',
    lhablock = 'MASS',
    lhacode = [41])
 
MAh2 = Parameter(name = 'MAh2',
    nature = 'internal',
    type = 'real',
    value = 'M3',
    texname = 'M_{{a}_2}',
    lhablock = 'MASS',
    lhacode = [39])
 
MHp2 = Parameter(name = 'MHp2',
    nature = 'internal',
    type = 'real',
    value = 'M3',
    texname = 'M_{{H^+}_2}',
    lhablock = 'MASS',
    lhacode = [37])
 
MHp3 = Parameter(name = 'MHp3',
    nature = 'internal',
    type = 'real',
    value = 'M5',
    texname = 'M_{{H^+}_3}',
    lhablock = 'MASS',
    lhacode = [43])

  MHmm = Parameter(name = 'MHmm',
      nature = 'external',
      type = 'real',
      value = 'M5',
      texname = 'M_{H^{--}}', 
      lhablock = 'MASS',
      lhacode = [45])
*)
