zers=Sequence[DependenceNum -> 0.,
              Dependence -> 0.,
              DependenceOptional -> 0.];

dep[expr_]:=Sequence[DependenceNum -> expr,
              DependenceSPheno -> expr,
              Dependence -> None,
              Value-> None,
              DependenceOptional -> expr];
delts[i_]:=Sequence[
              Form -> Diagonal,
              DependenceNum -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              DependenceSPheno -> N[Table[KroneckerDelta[a,b],{a,i},{b,i}]],
              Dependence -> Table[KroneckerDelta[a,b],{a,i},{b,i}],
              DependenceOptional -> Table[KroneckerDelta[a,b],{a,i},{b,i}]];

six2twoM=Table[s2t[i, j] (KroneckerDelta[3, i, j] + KroneckerDelta[6, i, j] + KroneckerDelta[3, i] KroneckerDelta[6, j] +KroneckerDelta[3, j] KroneckerDelta[6, i]),{i,6},{j,6}];    
six2two[symb_]:=Sequence[
              Dependence -> (six2twoM/.s2t->symb),
              DependenceNum -> (six2twoM/.s2t->symb),
              DependenceOptional -> (six2twoM/.s2t->symb)];

massdep[field_,v_:v]:={{(Sqrt[2]*Mass[field, 1])/v, 0, 0}, {0, (Sqrt[2]*Mass[field, 2])/v, 0}, {0, 0, (Sqrt[2]*Mass[field, 3])/v}};


(*this parametrisation uses Mh1 Mh2, M1, M2 M3, M5 and alphaH, SinH as inputs while all dimensionless couplings in the lagrangian are derved quantities *)
ParameterDefinitions = { 
 
{g1,        { Description -> "Hypercharge-Coupling"}},
{g2,        { Description -> "Left-Coupling"}},
{g3,        { Description -> "Strong-Coupling"}},    
{AlphaS,    {Description -> "Alpha Strong"}},	
{e,         { Description -> "electric charge"}}, 

{Gf,        { Description -> "Fermi's constant"}},
{aEWinv,    { Description -> "inverse weak coupling constant at mZ"}},

                                                                         
{mu2,{ 
    Description -> "SM Mu Parameter", 
    LesHouches -> {HMIX, 2}, 
    OutputName -> Mu}},

{Meta, {Real -> True, LesHouches -> {MINPAR, 1}, LaTeX -> "M_\\eta", OutputName -> Meta, Value->  (400.0)^2 }},
{M5,   {Real -> True, LesHouches -> {MINPAR, 5}, LaTeX -> "M_5",     OutputName -> M5,   Value -> 600.0}},
{M2,   {Real -> True, LesHouches -> {MINPAR, 2}, LaTeX -> "M_2",     OutputName -> M2,   Value -> 600.0 }},


{Lambda1, {Real -> True, DependenceNum -> Mass[hh, 1]^2/vSM^2, OutputName -> Lam1, LaTeX ->"\\lambda_1"}},
{Lambda2, {Real -> True, DependenceNum -> (2*M5^2 - 3*Meta + Mass[hh, 2]^2)/(6*vSM^2), OutputName -> Lam2, LaTeX ->"\\lambda_2"}},
{Lambda5, {Real -> True, DependenceNum -> (2*(Mass[hh, 3]^2 - Mass[hh, 2]^2))/(3*vSM^2), OutputName -> Lam5, LaTeX ->"\\lambda_5"}},
{Lambda5c, {Real -> True, dep[Lambda5], OutputName -> Lam5c, LaTeX ->"\\lambda_5c"}},

{Lambda3,{
     Real -> True,
     LesHouches -> {MINPAR,3} ,
     Value -> -0.1,
     LaTeX -> "{\\lambda}_{3}" ,
     OutputName -> Lam3 }},

{Lambda4,{ 
     Real -> True,
     LesHouches -> {MINPAR,4} ,
     Value -> 0.2,
     LaTeX -> "{\\lambda}_{4}" , 
     OutputName -> Lam4 }},

{ThetaW,    { Description -> "Weinberg-Angle",
              DependenceNum -> ArcSin[Sqrt[1 - Mass[VWp]^2/Mass[VZ]^2]]  }},

{ZZ, {Description -> "Photon-Z Mixing Matrix"}},
{ZW, {Description -> "W Mixing Matrix", Dependence -> 1/Sqrt[2] {{1, 1}, {\[ImaginaryI],-\[ImaginaryI]}} }},

{vSM, {Description -> "EW-VEV",
      Real->True,
      dep[2*Mass[VWp]/e*Sqrt[1-Mass[VWp]^2/Mass[VZ]^2]],
      LaTeX -> "v_{SM}",
      OutputName -> vSM}},

{vDoub,{Real -> True, dep[vSM], OutputName -> vDoub, LaTeX -> "{v}_{\\Phi}"}},


{ZH, {Real -> True, Description -> "Scalar-Mixing-Matrix",dep[{{1,0,0},{0,1/Sqrt[3],Sqrt[2/3]},{0,-Sqrt[(2/3)],1/Sqrt[3]}}], OutputName -> ZH}}, 

{ZP, {Real -> True, Description -> "Charged-Mixing-Matrix", dep[{{1, 0, 0}, {0, 1/Sqrt[2], 1/Sqrt[2]}, {0, -(1/Sqrt[2]), 1/Sqrt[2]}}], OutputName -> ZP}},

{ZA, {Real -> True, Description -> "Pseudo-Scalar-Mixing-Matrix", delts[2], OutputName -> ZA}}, 

{Yu,        { Description -> "Up-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fu, vDoub],
			 DependenceNum -> massdep[Fu, vDoub]}},
             									
{Yd,        { Description -> "Down-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fd, vDoub],
			 DependenceNum -> massdep[Fd, vDoub]}},
              									
{Ye,        { Description -> "Lepton-Yukawa-Coupling",
             Form -> Diagonal,
             Real -> True,
             Dependence -> massdep[Fe, vDoub],
			 DependenceNum -> massdep[Fe, vDoub]}},

{Vu,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Up-Mixing-Matrix"}},
{Vd,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Down-Mixing-Matrix"}},
{Uu,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Up-Mixing-Matrix"}},
{Ud,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Down-Mixing-Matrix"}}, 
{Ve,        {delts[3], Real->True,Form->Diagonal, Description ->"Left-Lepton-Mixing-Matrix"}},
{Ue,        {delts[3], Real->True,Form->Diagonal, Description ->"Right-Lepton-Mixing-Matrix"}}};

(* The following mass definitions need to be added/replaced by hand. In addition Meta->Meta^2 is replace everywhere
    
Mh3 = Parameter(name = 'Mh3',
    nature = 'internal',
    type = 'real',
    value = 'M5',
    texname = 'M_{{h}_3}',
    lhablock = 'MASS',
    lhacode = [41])

M3 = Parameter(name = 'M3',
    nature = 'internal',
    type = 'real',
    value = '1/3*(M5**2 + 2*Mh2**2)',
    texname = 'M_{{H^+}_2}',
    lhablock = 'MASS',
    lhacode = [37])

MAh2 = Parameter(name = 'MAh2',
    nature = 'internal',
    type = 'real',
    value = 'M3',
    texname = 'M_{{a}_2}',
    lhablock = 'MASS',
    lhacode = [39])

MHp2 = Parameter(name = 'MHp2',
    nature = 'internal',
    type = 'real',
    value = 'M3',
    texname = 'M_{{H^+}_2}',
    lhablock = 'MASS',
    lhacode = [37])

MHp3 = Parameter(name = 'MHp3',
    nature = 'internal',
    type = 'real',
    value = 'M5',
    texname = 'M_{{H^+}_3}',
    lhablock = 'MASS',
    lhacode = [43])

MHmm = Parameter(name = 'MHmm',
    nature = 'internal',
    type = 'real',
    value = 'M5',
    texname = 'M_{H^{--}}',
    lhablock = 'MASS',
    lhacode = [45])
*)
