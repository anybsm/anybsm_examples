# Generation of UFO models for anyBSM

*  Each directory consists of a SARAH model (`<model-name>.m`, `parameters.m` and `particles.m`) as well as a Mathematica notebook `<model-name>_ModelOutput.nb`. The latter loads the SARAH model, works-out the relation between Lagrangian- and input-parameters (which have already been saved in the `parameters.m`) and creates the UFO-files using `MakeUFO`. In order to re-generate the UFO models, the path to the `SARAH_modelfiles` directory must be present in the variable `SARAH[InputDirectories]` which is set in the file `SARAH.config` in your SARAH installation.
* `FeynRules_modelfiles`  
   Each directory consists of a FeynRules model (`<modelname>.fr` plus possible restriction files) as well as an Bash-script `gen_<model-name>`. The script loads FeynRules and generates the UFO output using `WriteUFO`. Afterwards the resulting model needs to be converted using the executable `anyBSM_convert`. For FeynRules to work, the absolute paths within the script need to be adapted.
  

All files can be downloaded [here](https://gitlab.com/anybsm/anybsm_examples/-/tree/main/ModelfileGeneration).   
General documentation on how-to generate UFO model is located [here](https://anybsm.gitlab.io/index.html#setup-a-new-model).
