# anyBSM Example Repository
This is a collection of examples for various aspects of the program [anyBSM](anyBSM/anyBSM) all files are hosted [here](https://gitlab.com/anybsm/anybsm_examples).

## General structure

 * [MathematicaExamples](https://anybsm.gitlab.io/examples/MathematicaExamples.html)
   * Example files on how to use the mathematica interface of anyBSM.
   * Analytic cross-checks (such as UV-finiteness of the one-loop renormalized trilinear Higgs coupling) in various BSM models.   
  
 * [PythonExamples](https://anybsm.gitlab.io/examples/PythonExamples.html)
   * Jupyter notebooks with basic as well as advanced examples.  

 * [ModelfileGeneration](https://gitlab.com/anybsm/anybsm_examples/-/tree/main/ModelfileGeneration)
   * Examples for generating anyBSM-compatible UFO models using FeynRules or SARAH model files.  
 
