# anyBSM Python Jupyter Notebook Examples

Various python examples for anyBSM use cases.  
The Jupyter notebooks and all other contents can be downloaded [here](https://gitlab.com/anybsm/anybsm_examples/-/tree/main/PythonExamples).  
  
A selection of well-documented examples reads:
 * [AnyBSM_Tutorial.ipynb](https://anybsm.gitlab.io/examples/PythonExamples/AnyBSM_Tutorial.html)  
   Tutorial for basic usage of`anyBSM`. This should be a good starting point for beginners.
 * [AnyExample_UncertaintyEstimates.ipynb](https://anybsm.gitlab.io/examples/PythonExamples/AnyExample_UncertaintyEstimates.html)  
   Estimate of missing higher-order uncertainties in the SM and IDM.
 * [AnyExample_Decoupling.ipynb](https://anybsm.gitlab.io/examples/PythonExamples/AnyExample_Decoupling.html)  
   Show/check proper decoupling for various BSM models.
 * [AnyExample_NonDecoupling.ipynb](https://anybsm.gitlab.io/examples/PythonExamples/AnyExample_NonDecoupling.html)  
   Show large mass-splitting effects for various BSM models.
 * [AnyExample_THDMII_OStadpoles.ipynb](https://anybsm.gitlab.io/examples/PythonExamples/AnyExample_THDMII_OStadpoles.html)  
   Stability-test of tadpole schemes in the THDM-II.
 * [AnyExample_MSSM_SLHA.ipynb](https://anybsm.gitlab.io/examples/PythonExamples/AnyExample_MSSM_SLHA.html)  
   Shows how to connect `anyBSM` to a SPheno spectrum generator and read-in SLHA files at the example of the MSSM.
 * [AnyExample_THDMI_momdep.ipynb](https://anybsm.gitlab.io/examples/PythonExamples/AnyExample_THDMI_momdep.ipynb)  
   Studies effect of external momentum onto the trilinear Higgs self-coupling in the THDM-I.
 * [AnyExample_SSM.ipynb](https://anybsm.gitlab.io/examples/PythonExamples/AnyExample_SSM.ipynb)  
   Computes the trilinear self-coupling of a BSM scalar in addition to the SM-like Higgs in the singlet extended SM.

More examples are listed [here](https://anybsm.gitlab.io/examples/PythonExamples.html) in the navigation on the left-hand side. 
